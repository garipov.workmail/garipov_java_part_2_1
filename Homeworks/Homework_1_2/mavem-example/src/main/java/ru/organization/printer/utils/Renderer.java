package ru.organization.printer.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.BufferedInputStream;

public class Renderer {
	public void print(String fileName, char white, char black){
		try { 

			BufferedInputStream file = new BufferedInputStream(getClass().getResourceAsStream(fileName));

			BufferedImage image = ImageIO.read(file);
			
			int height = image.getHeight();
			int width = image.getWidth();

			for (int i = 0; i < height; i++){
				for (int j = 0; j < width; j++){
					if (image.getRGB(j, i) == Color.BLACK.getRGB()) {
						System.out.print(black);
					}else{
						System.out.print(white);
					}
				}
				System.out.println();
			 }
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}

	}
}
 