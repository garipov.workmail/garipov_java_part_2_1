package ru.organization.printer.app;

import ru.organization.printer.utils.Renderer;
import com.beust.jcommander.JCommander;

class Program {
	public static void main(String[] args) {
		Arguments arguments = new Arguments();

		JCommander.newBuilder()
			.addObject(arguments)
			.build()
			.parse(args);

		Renderer renderer = new Renderer();
		renderer.print("image.bmp", arguments.white.charAt(0), arguments.black.charAt(0)); 
	}
}