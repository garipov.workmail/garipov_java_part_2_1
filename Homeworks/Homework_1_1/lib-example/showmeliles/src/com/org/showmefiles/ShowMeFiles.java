package com.org.showmefiles;

import java.io.File;

public class ShowMeFiles {
    public void print(String path) {
        //Создание объекта для работы с файлами и каталогами
        File file = new File(path);
        //Получение массива с именами файлов и каталогов
        String[] fileList = file.list();
        //Вывод информации с добавлением размера файла или каталога
        for (int i = 0; i < fileList.length; i++) {
            //Запрос имени файла или каталога для получения размера
            String pathfile = fileList[i];
            //Создание объекта size типа File для получения размера
            File size = new File(path, pathfile);
            //Вывод информации
            System.out.println(fileList[i] + " " + size.length());
        }
    }
}
