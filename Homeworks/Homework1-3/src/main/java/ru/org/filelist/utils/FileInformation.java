package ru.org.filelist.utils;

import ru.org.filelist.app.Arguments;
import ru.org.filelist.app.ThreadService;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileInformation  {



    public Map<String, List<String>> getFileInformation(Arguments arguments) {

        //Create map for file list information
        Map<String, List<String>> map = new HashMap<>();
        //Upload data in map
        for (int i = 0; i < arguments.path.size(); i++) {
            map.put(arguments.path.get(i), new ArrayList<>());
        }

        ThreadService threadService = new ThreadService();

            for (int i = 0; i < arguments.path.size(); i++) {
                int finalI = i;
                threadService.submit(() -> {
                File file = new File(arguments.path.get(finalI));
                String[] fileList = file.list();
                for (int j = 0; j < fileList.length; j++) {
                    map.get(arguments.path.get(finalI)).add(fileList[j]);
                }
                });

            }


        return map;
    }


}
