package ru.org.filelist.app;


import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters(separators = "=")

public class Arguments {

    @Parameter(names = "--path")
    public List<String> path;
}
