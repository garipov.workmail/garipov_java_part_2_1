package ru.org.filelist.app;

import com.beust.jcommander.JCommander;
import ru.org.filelist.utils.FileInformation;



import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Arguments arguments = new Arguments();

        JCommander.newBuilder().
                addObject(arguments).
                build().
                parse(args);

        FileInformation fileInformation = new FileInformation();


        for (Map.Entry entry : fileInformation.getFileInformation(arguments).entrySet()) {
            System.out.println("Key: " + entry.getKey() + " Value: "
                    + entry.getValue());
        }

    }
}
